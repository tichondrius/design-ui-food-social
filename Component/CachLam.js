import FadeInView from '../shared/fade-in-view'
import React, { Component } from 'react';
import { TouchableNativeFeedback, View, Image, Text, StyleSheet, TouchableOpacity, Dimensions, TextInput , UselessTextInput, Alert, Button } from 'react-native'
import { truncate } from '../libary'
const { height } = Dimensions.get('window');

export default class CachLam extends Component{

constructor(){
    super()
    this.state = {
        viewSection :false,
        viewSection2 :false,
        conpo: '{this.renderBottomComponent()}',
        number: 1
    }
}

renderBottomComponent=()=>{
    if(this.state.viewSection) {
        return (
            <View style={{flexDirection: 'row'}}>
                <TextInput style={{height: height / 20, width: 290, backgroundColor:'white', fontSize: 15, marginLeft: 5}} underlineColorAndroid='transparent' placeholder="điền theo ví dụ: 2gr đường">
                </TextInput>
                <TouchableOpacity>
                    <Image source={require('../images/recycle-bin.png')} style={txt.images}/>
                  </TouchableOpacity>
              </View> 
        )
    }
}

renderBottomComponent2=()=>{
    if(this.state.viewSection2) {
        return (

            <View style={{flexDirection: 'row'}}>
                  <Text style={{fontSize: 18, fontWeight: 'Bold', marginLeft: 5}}>Bước 2: </Text>
                  <TextInput style={{height: 'auto', width: 195, backgroundColor:'white', fontSize: 16, textAlignVertical: 'top', marginLeft: 5}} 
                  multiline={true} underlineColorAndroid='transparent'
                  >
                  </TextInput>
                  <TouchableOpacity>
                    <Image source={require('../images/camera-ico.png')} style={txt.images}/>
                  </TouchableOpacity>
                  <TouchableOpacity>
                    <Image source={require('../images/recycle-bin.png')} style={txt.images}/>
                  </TouchableOpacity>
              </View>
        )
    }
}

buttonPress=()=>{
    this.setState({viewSection:true})
}

buttonPress2=()=>{
    this.setState({viewSection2:true})
}
  render(){
    
    return (
      <FadeInView style={{height: 'auto', marginLeft: 3, marginTop: 3, marginRight: 3, borderRadius: 1, marginBottom: 3, elevation: 2, backgroundColor:'#ffffaa'}}>
         
           <View>
             <TextInput style={txt.textinputHeader} placeholder="Đặt tên món ăn" underlineColorAndroid='transparent'>
             </TextInput>
             <TextInput style={txt.textinput} placeholder="Giới thiệu một chút về món ăn nhé ..." underlineColorAndroid='transparent'>
             </TextInput>
           </View>

          <View style={{flexDirection: 'row'}}>
             <Text style={{fontSize: 20, fontWeight: 'Bold', flex: 6, marginLeft: 5}}>Nguyên liệu</Text>
              <View style={{flex: 5}}>
                <TextInput style={{height: height / 20, width: 143, backgroundColor:'#ffffaa', fontSize: 15}} placeholder="Bao nhiêu phần ăn?" underlineColorAndroid='transparent'>
                </TextInput>
              </View>          
          </View>

          <View style={txt.nen}>

              <View style={{flexDirection: 'row'}}>
                <TextInput style={{height: height / 20, width: 290, backgroundColor:'white', fontSize: 15, marginLeft: 5}} placeholder="điền theo ví dụ: 2gr đường" underlineColorAndroid='transparent'>
                </TextInput>
                <TouchableOpacity>
                    <Image source={require('../images/recycle-bin.png')} style={txt.images}/>
                </TouchableOpacity>
              </View>

              
               {this.renderBottomComponent()}
          </View>

          <TouchableOpacity onPress={this.buttonPress}>
            <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
              <Text style={{fontSize: 20, fontWeight: 'Bold'}}>+ </Text>
              <Text style={{fontSize: 15, fontWeight: 'Bold'}}>Thêm nguyên liệu</Text>
            </View>            
          </TouchableOpacity>

            <Text style={{fontSize: 20, fontWeight: 'Bold', marginLeft: 5}}>Cách làm</Text>

            <View style={txt.nen}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={{fontSize: 18, fontWeight: 'Bold', marginLeft: 5}}>Bước {this.state.number}: </Text>
                  <TextInput style={{height: 'auto', width: 195, backgroundColor:'white', fontSize: 16, textAlignVertical: 'top', marginLeft: 5} } 
                  multiline={true} underlineColorAndroid='transparent'
                  >
                  </TextInput>
                  <TouchableOpacity>
                    <Image source={require('../images/camera-ico.png')} style={txt.images}/>
                  </TouchableOpacity>
                  <TouchableOpacity>
                    <Image source={require('../images/recycle-bin.png')} style={txt.images}/>
                  </TouchableOpacity>
                </View>
                {this.renderBottomComponent2()}
          </View>

           <TouchableOpacity onPress={this.buttonPress2}>
            <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
              <Text style={{fontSize: 20, fontWeight: 'Bold'}}>+ </Text>
              <Text style={{fontSize: 15, fontWeight: 'Bold'}}>Thêm Cách làm</Text>
            </View>
          </TouchableOpacity>
        
      </FadeInView> 
        
     );
  }  
}

const txt = StyleSheet.create({
  wrapper: {height: height / 8, backgroundColor: '#ffffaa', padding: 10, justifyContent: 'space-around'},
  textinputHeader: {height: height / 15, backgroundColor:'#ffffaa', fontSize: 20, marginLeft: 5},
  textinput: {height: height / 15, backgroundColor:'#ffffaa', fontSize: 15, marginLeft: 5},
  nen:{width: 320, height: 'auto', backgroundColor: "white", marginLeft: 16},
  images:{width: 25, height: 25, justifyContent: 'center', alignItems: 'center'}
});