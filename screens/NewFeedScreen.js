import React, { Component } from 'react';
import { MaterialIcons, MaterialCommunityIcons } from '@expo/vector-icons'
const uuid = require('uuid');

import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    ListView,
    TouchableHighlight,
    TouchableNativeFeedback,
    Alert,
    alertMessage,
    Animated,
    ToolbarAndroid,
    Button,
    ProgressBarAndroid,
    Picker,
    RefreshControl,
    ViewPagerAndroid,
    ActivityIndicator
} from 'react-native';
import { BaseButton } from 'react-native-gesture-handler';
import CategoryItem from '../category/category-item';
import { HeaderButtons } from '../shared';




export default class NewFeedScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: `Home`,
    tabBarIcon: ({ tintColor }) => (
      <BaseButton><MaterialCommunityIcons name="earth" size={25} color={tintColor}/></BaseButton>
    ),
    tabBarLabel: 'Tin tức',
    headerTitle: (props) => <HeaderButtons {...props} navigation={navigation}/>,
    headerTitleStyle: {marginRight: 10, color: "white"},
    headerStyle: style.navbar,
    headerLeft : <MaterialIcons.Button
    color="white"
    style={{paddingRight: 0}}
    name="menu"
    backgroundColor="#11c1f2"
    size={30}
    onPress={()=>{navigation.navigate('DrawerOpen')}} />
  });
  // Initialize the hardcoded data
  constructor(props) {
    
    
    super(props);
    
    this.setInitState();
  }
  setInitState(){
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1._id !== r2._id});
    this.state = {
      dataSource: ds.cloneWithRows([
        
      ]),
      categories: [],
      firstLoad: false,
      isloadingmore: false,
      isRefreshing: false,
      isEndItem: false
    };
  }
  
  
  componentDidMount(){
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1._id !== r2._id});
    setTimeout(() => {
      const results = require('../mockJSON/Feeds.json').map(h => ({
        ...h,
        _id: uuid(),
      }));

      this.setState({ dataSource: this.state.dataSource.cloneWithRows(results), firstLoad: true, categories: results})
    }, 500);
  }
  _onRefresh = () => {
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1._id !== r2._id});
    this.setState({isRefreshing: true});
    this.setState({
      dataSource: ds.cloneWithRows([
        
      ]),
      categories: [],
      isloadingmore: false,
      isEndItem: false
    });
    setTimeout(() => {
      const results = require('../mockJSON/Feeds.json').map(h => ({
        ...h,
        _id: uuid(),
      }));

      this.setState({dataSource: this.state.dataSource.cloneWithRows(results), firstLoad: true, categories: results, isRefreshing: false});
    }, 500);
  };

  renderFooter(){
    let button = null;
    if (this.state.isEndItem == false && this.state.isRefreshing == false && (this.state.isloadingmore == true || this.state.firstLoad && this.state.firstLoad == true)){
      button = <ActivityIndicator
              style={[Indicatorstyles.centering, {transform: [{scale: 1.4}]}]}
              size="large"
              color="#32c5d2"
          />;
    }
    return button;
  }
  handlerEndReached(){
     if (this.state.isloadingmore == true) return;
     this.setState({isloadingmore: true});
     let curQty = this.state.dataSource.getRowCount();
     
     const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1._id !== r2._id});

     setTimeout(() => {
      const results = require('../mockJSON/Feeds.json').map(h => ({
        ...h,
        _id: uuid(),
      }));

      this.state.categories = this.state.categories.concat(results);
      this.setState({dataSource: this.state.dataSource.cloneWithRows(this.state.categories), isloadingmore: false});
    }, 500);
  }
  render() {
   let processBar = null;
   if (this.state.firstLoad == false){
     processBar =  <ActivityIndicator
              style={[Indicatorstyles.centering, {transform: [{scale: 1.4}]}]}
              size="large"
              color="#32c5d2"
          />;
   }
    return (
        <View style={{flex: 1}}>
          {processBar}
          <ListView
            refreshControl={
            <RefreshControl
              refreshing={this.state.isRefreshing}
              onRefresh={this._onRefresh}
              tintColor="#ff0000"
              title="Loading..."
              titleColor="#00ff00"
              colors={['#11c1f2','#00ff00', '#32c5d2']}
              progressBackgroundColor="white"
            />}
            dataSource={this.state.dataSource}
            renderRow={(rowData) => <CategoryItem category={rowData} navigation={this.props.navigation} />}
            renderFooter={this.renderFooter.bind(this)}
            onEndReached={this.handlerEndReached.bind(this)}
            onEndReachedThreshold={10}
          />
        </View>
    );
  }
}
const style = StyleSheet.create({
  navbar: {
    backgroundColor: '#11c1f2'
  },
  title: {
    fontSize: 20,
    fontWeight: '500',
    color: 'white',
    textAlign: 'left',
    marginHorizontal: 16,
  }
})
const Indicatorstyles = StyleSheet.create({
  centering: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 8,
  }
});