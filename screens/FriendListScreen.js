import React, { Component } from 'react';
import { MaterialIcons, MaterialCommunityIcons, Entypo } from '@expo/vector-icons'

import {
    StyleSheet,
    Text,
    View,
} from 'react-native';
import { BaseButton } from 'react-native-gesture-handler';
import { HeaderButtons } from '../shared';


export default class FriendListScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: `Home`,
    tabBarIcon: ({ tintColor }) => (
      <BaseButton><Entypo name="users" size={25} color={tintColor}/></BaseButton>
    ),
    tabBarLabel: 'Bạn bếp',
    headerTitle: (props) => <HeaderButtons {...props} navigation={navigation}/>,
    headerTitleStyle: {marginRight: 10, color: "white"},
    headerStyle: style.navbar,
    headerLeft : <MaterialIcons.Button
    color="white"
    style={{paddingRight: 0}}
    name="menu"
    backgroundColor="#11c1f2"
    size={30}
    onPress={()=>{navigation.navigate('DrawerOpen')}} />
    
  });
  // Initialize the hardcoded data
  constructor(props) {
    super(props);
  }
  render() {
      return (
        <View>
            <Text>Friend list</Text>
        </View>
      )
    
  }
}
const style = StyleSheet.create({
  navbar: {
    backgroundColor: '#11c1f2'
  }
});