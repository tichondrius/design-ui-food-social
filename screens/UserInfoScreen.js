import React from 'react';
import { View, Text, StyleSheet } from 'react-native';


export default class UserInfoScreen extends React.Component {

    static navigationOptions = ({ navigation: { state: {params: { NameUser }}} }) => ({
        title: `Trang của ${NameUser}`,
    });
    render() {
        const { NameUser } = this.props.navigation.state.params;
        return (
            <View style={styles.container}>
                <Text>Thông tin của {NameUser}</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 5,
    }
});