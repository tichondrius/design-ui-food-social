import React, { Component } from 'react';
import { MaterialIcons, FontAwesome } from '@expo/vector-icons'
const uuid = require('uuid');

import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  ListView,
  TouchableHighlight,
  TouchableNativeFeedback,
  Alert,
  alertMessage,
  Animated,
  ToolbarAndroid,
  Button,
  ProgressBarAndroid,
  Picker,
  RefreshControl,
  ViewPagerAndroid,
  TextInput,
  ActivityIndicator
} from 'react-native';

import CategoryItem from '../category/category-item'
import {truncate} from '../libary'


export default class SearchScreen extends Component{
  static navigationOptions = ({ navigation }) => ({
    title: `Tìm kiếm`,
    header: null,
    headerTitleStyle: {marginRight: 10, color: "white"},
    headerStyle: style.navbar,

  });
  ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1._id !== r2._id});
  constructor(props){
    super(props);
     
      this.state = {
        dataSource: this.ds.cloneWithRows([
          
        ]),
        categories: [],
        isloadingmore: false,
        isEndItem: false,
        isSearching: false,
        keyword: '',
        currentSearch: -1
    };
    this.onFetchSearch = this.onFetchSearch.bind(this);
    this.onChangeKeyword = this.onChangeKeyword.bind(this); 
  }

  renderFooter(){
      let button = null;
      if (this.state.isSearching == true){
        button = <ActivityIndicator
                style={[Indicatorstyles.centering, {transform: [{scale: 1.4}]}]}
                size="large"
                color="#32c5d2"
            />;
      }
      return button;
    }
  onChangeKeyword(text){
      console.log(text);
      this.setState({keyword: text});
      console.log(text);
        this.onFetchSearch();
      
  }
  onFetchSearch(){
     let currentSearch = this.state.currentSearch + 1;
     this.setState({isSearching: true, currentSearch: currentSearch,  dataSource: this.ds.cloneWithRows([
          
        ]),
        categories: []});

    setTimeout(() => {
        const results = require('../mockJSON/Feeds.json').map(feed => ({
            ...feed,
            foodName: this.state.keyword,
            _id: uuid(),
        }));
        this.setState({dataSource: this.state.dataSource.cloneWithRows(results), isSearching: false});
    }, 500);
  }
  render(){
    let {navigate, goBack} = this.props.navigation;
     return ( <View style={{flex: 1}}>
       <View style={{height:25, elevation: 2,  width: '100%', backgroundColor: "#11c1f2", flexDirection: 'row'}}></View>
       <View style={{height:50, elevation: 2,  width: '100%', backgroundColor: "#11c1f2", flexDirection: 'row'}}>
        
        <View style={{flex: 2, alignItems: 'center', justifyContent: 'center'}}>
            <MaterialIcons.Button onPress={() => goBack()} backgroundColor="#11c1f2" onTouch={() => {}} name="arrow-back" size={24} color='white'/>
        </View>
        <View style={{flex: 9, justifyContent: 'center'}}>
             <TextInput 
                style={{fontSize: 18, borderRadius: 5, margin: 3, backgroundColor: "white", padding: 5, elevation: 2}}
                autoFocus={true}
                placeholder ={"Tìm kiếm món ăn, bài đăng"}
                autoCorrect={true}
                onChangeText={this.onChangeKeyword}
                value={this.state.keyword}
                underlineColorAndroid='rgba(0,0,0,0)'
                />
        </View>
          <View style={{flex: 2, alignItems: 'center', justifyContent: 'center'}}>
            <MaterialIcons.Button onPress={() => this.setState({keyword: ''})} backgroundColor="#11c1f2" onTouch={() => {}} name="close" size={24} color='white'/>
            </View>
       
        </View>
            <ListView
              dataSource={this.state.dataSource}
              renderRow={(rowData) => <CategoryItem category={rowData} navigation={this.props.navigation} />}
              renderFooter={this.renderFooter.bind(this)}
            />
          
        </View>
          );
  }
}
const style = StyleSheet.create({
  navbar: {
    backgroundColor: '#32c5d2'
  }
})

const Indicatorstyles = StyleSheet.create({
  centering: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 8,
  }
});