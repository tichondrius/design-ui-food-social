import React from 'react';
import { View, Text, StyleSheet, Dimensions, Image } from 'react-native';
import ImageView from 'react-native-image-view';

const { width, height } = Dimensions.get('window');



const foodImages = [
  require('../images/1.png'),
  require('../images/2.png'),
  require('../images/3.png'),
  require('../images/4.png'),
  require('../images/5.png'),
];


export default class ViewImageScreen extends React.Component {

    static navigationOptions = ({ navigation: { state: {params: { tutorialDesc, image }}} }) => ({
        title: tutorialDesc,
    });
    render() {
      
      const { navigation: { state: {params: { tutorialDesc, image }}} } = this.props;
        const { NameUser } = this.props.navigation.state.params;
        return (
            <View style={{ width, height }}>
            <ImageView
              onClose={() => {this.props.navigation.goBack()}}
              images={[{
                source: foodImages[image],
                title: "Image",
                width,
                height: 300,
              }]}
              imageIndex={0}
              isVisible={true}
            />
            </View>
        )
    }
}
