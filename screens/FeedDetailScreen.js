import React from 'react';
import { View, Text, StyleSheet, Image, ScrollView, Dimensions, Button, TouchableOpacity } from 'react-native';

import { Entypo, FontAwesome, MaterialIcons, Ionicons } from '@expo/vector-icons';


import Toast, {DURATION} from 'react-native-easy-toast'

const { width } = Dimensions.get('window');
const avatars = [
    require('../images/chibao.png'),
    require('../images/thanhlai.png'),
    require('../images/vanbui.png'),
    require('../images/baotrang.png'),
    require('../images/diemchau.png'),
  ];
  
  const foodImages = [
    require('../images/1.png'),
    require('../images/2.png'),
    require('../images/3.png'),
    require('../images/4.png'),
    require('../images/5.png'),
  ];

const tutorialsData = [{
    id: 1,
    part: 1,
    description: 'Mực rửa qua nước pha muối rồi rửa lại cho sạch, vẩy ráo nước, cắt miếng vừa ăn, ướp tí muối , tí bột ngọt',
    image: 0,
}, {
    id: 2,
    part: 2,
    description: 'Thơm, cà, rau cần rửa sạch cắt miếng vừa ăn',
    image: 1,
}, {
    id: 3,
    part: 3,
    description: 'Bắc chảo với một muỗng canh dầu ăn phi thơm tỏi cho mực, thơm, cà lên xào, nêm vừa ăn thêm tiêu, rau cần',
    image: 2,
}, {
    id: 4,
    part: 4,
    description: 'Thơm, cà, rau cần rửa sạch cắt miếng vừa ăn',
    image: 3,
}, {
    id: 5,
    part: 5,
    description: 'Mực rửa qua nước pa muối rồi rửa lại cho sạch, vẩy ráo nước, cắt miếng vừa ăn, ướp tí muối , tí bột ngọt',
    image: 4,
}]

export default class FeedDetailScreen extends React.Component {

    static navigationOptions = ({ navigation: { state: { params: { category }}}, navigation }) => ({
        title: `Chi tiết - ${category.foodName}`,
        headerRight:   <Ionicons.Button onPress={() => {
            navigation.setParams({
                category: {
                    ...category,
                like: !category.like,
                likeCount: category.like ? category.likeCount - 1: category.likeCount + 1,
                }
            });
            if (this.toast) {
                this.toast.show(`Bạn đã ${!category.like ? '': 'bỏ'} thích bài viết`,DURATION.LENGTH_SHORT);
            }
        }} size={25} backgroundColor="#11c1f2" color={category.like ? 'red' : '#a6a6a6'} name="ios-heart"><Text style={{color:'white'}}>{category.likeCount}</Text></Ionicons.Button>
    });

    render() {
        const { state: { params: { category }}} = this.props.navigation;
        return (
            <ScrollView style={{display: 'flex'}}>
                <ScrollView
                    style={{width: '100%', height: 250}}
                    horizontal
                    pagingEnabled
                    showsHorizontalScrollIndicator={true}
                >
                     <Image
                        resizeMode='cover'
                        style={styles.imageInfo}
                        source={foodImages[category.image]}
                    />
                    <Image
                        resizeMode='cover'
                        style={styles.imageInfo}
                        source={foodImages[category.image + 1 > 4 ? 0 : category.image + 1]}
                    />
                    <Image
                        resizeMode='cover'
                        style={styles.imageInfo}
                        source={foodImages[category.image + 2 > 4 ? 0 : category.image + 2]}
                    />
                </ScrollView>
               
                <View style={styles.container}> 
                <Toast
                    ref={(element) => this.toast = element}
                    style={{backgroundColor:'black'}}
                    position='top'
                    positionValue={200}
                    fadeInDuration={750}
                    fadeOutDuration={1000}
                    opacity={0.8}
                    textStyle={{color:'white'}}
                />
                    <Text style={styles.foodName}>{category.foodName}</Text>
                    <Text style={styles.foodDescrible}>Thơm ngon giòn xốp dễ làm lắm nha mọi người cứ theo hướng dẫn</Text>
                
                    <View style={styles.ownerWrapper}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate("UserInfoScreen", {
                            category,
                            NameUser: category.ownerName
                        })}>
                            <View style={styles.ownerInfo}>
                                <Image source={avatars[category.avatar]} resizeMode='contain' style={styles.ownerAvatar} />
                                <Text style={styles.ownerName}>
                                    {category.ownerName}
                                </Text>
                            </View>
                        </TouchableOpacity>
                        <View style={styles.actions}>
                            <Button title="Kết bạn bếp"></Button>
                        </View>
                    </View>
                    <Text style={styles.h2}>Nguyên liệu</Text>
                    <View style={styles.parts}>
                        <Text style={styles.part}><Text style={styles.partBold}>270g</Text> bột mì 13</Text>
                        <Text style={styles.part}><Text style={styles.partBold}>1/4 trái</Text> thơm</Text>
                        <Text style={styles.part}><Text style={styles.partBold}>1 trái</Text> cà chua</Text>
                        <Text style={styles.part}><Text style={styles.partBold}>1 bó</Text> cần</Text>
                        <Text style={styles.part}><Text style={styles.partBold}>1/2 ký</Text> thịt heo</Text>
                        <Text style={styles.part}>Gia vị</Text>
                        <Text style={styles.part}>Rau thơm</Text>
                    </View>
                    <Text style={styles.h2}>Cách làm</Text>
                    {tutorialsData.map(tutorial => 
                    <View style={styles.tutorials} key={tutorial.id}>
                        <View style={styles.tutorial}>
                            <View style={styles.partWrapper}>
                                <Text style={styles.partText}>{tutorial.part}</Text>
                            </View>
                            <View>
                            <View style={styles.tutorialInfo}>
                                <Text style={styles.tutorialDesc}>{tutorial.description}</Text>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('ViewImageScreen', {
                                    tutorialDesc: tutorial.description,
                                    image: tutorial.image,
                                })}>
                                    <Image resizeMode="cover" source={foodImages[tutorial.image]} style={styles.tutorialImage}/>
                                </TouchableOpacity>
                            </View>
                            </View>
                        </View>
                    </View>)}
                </View>
            </ScrollView> 
        )
    }
}

const styles = StyleSheet.create({
    tutorialInfo: {
        flex: 1,
        width: width - 80,
        display: 'flex',
        flexDirection: 'column',
    },
    tutorialImage: {
        marginTop: 10,
        width: width - 80,
        height: 200,
    },
    partWrapper: {
        marginRight: 10,
        backgroundColor: '#AAAAAA',
        borderRadius: 5,
        width: 30,
        height: 30, 
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    partText: {
        color: 'white',
        fontSize: 15,
    },
    tutorialDesc: {
        fontSize: 17,
    },

    tutorials: {
        padding: 10,
        borderWidth: 1,
        borderColor: 'grey',
        borderRadius: 5,
        backgroundColor: 'white',
        marginBottom: 10,
    },
    tutorial: {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',

    },
    ownerWrapper: {
        marginTop: 10,
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    ownerInfo: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    ownerAvatar: {
        marginRight: 10,
        width: 50,
        height: 50,
        borderRadius: 50,
    },
    ownerName: {
        fontWeight: '600',
    },
    foodName: {
        fontSize: 20,
        color: '#11c1f2',
        fontWeight: '600',
    },
    foodDescrible: {
        fontSize: 16,
        marginTop: 15,
    },
    container: {
        backgroundColor: '#fffcfb',
        paddingTop: 5,
        paddingRight: 10,
        paddingLeft: 10,
    },
    imageInfo: {
        width: width,
        height: 250,
    },
    h2: {
        fontSize: 19,
        color: 'grey',
        marginTop: 10,
        marginBottom: 10,
        fontWeight: '600',
    },
    part: {
        color: '#606060',
        fontSize: 18,
    },
    parts: {
        padding: 10,
        borderWidth: 1,
        borderColor: 'grey',
        borderRadius: 5,
        backgroundColor: 'white',
    },
    partBold: {
        color: '#606060',
        fontSize: 18,
        fontWeight: '600',
    }
});