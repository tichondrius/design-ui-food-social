import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { MaterialIcons, MaterialCommunityIcons, Entypo, Feather, Ionicons } from '@expo/vector-icons'
import { Badge } from 'react-native-elements';


const HeaderButtons = ({ children, navigation }) => (
    <View style={styles.container}>
        <View><Text style={styles.title}>{children}</Text></View>
        <View style={{ flexDirection: 'row', marginRight: 10}}>
            <MaterialIcons.Button
            color="white"
            style={{paddingRight: 0}}
            name="search"
            backgroundColor="#11c1f2"
            size={25}
            onPress={()=>{navigation.navigate('Search')}} />
            <Entypo.Button
            color="white"
            style={{paddingRight: 0}}
            name="add-user"
            backgroundColor="#11c1f2"
            size={25}
            onPress={()=>{navigation.navigate('Search')}}>
                <Badge containerStyle={styles.badgeWrapper} value={3} textStyle={styles.badgeText}/>
            </Entypo.Button>
            
            <Feather.Button
            color="white"
            style={{paddingRight: 0}}
            name="message-circle"
            backgroundColor="#11c1f2"
            size={25}
            onPress={()=>{navigation.navigate('Search')}}>
                <Badge containerStyle={styles.badgeWrapper} value={3} textStyle={styles.badgeText}/>
            </Feather.Button>
            <Ionicons.Button
            color="white"
            style={{paddingRight: 0}}
            name="md-notifications"
            backgroundColor="#11c1f2"
            size={25}
            onPress={()=>{navigation.navigate('Search')}}>
                <Badge containerStyle={styles.badgeWrapper} value={3} textStyle={styles.badgeText}/>
            </Ionicons.Button>
        </View>
    </View>
);

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    title: {
        fontSize: 20,
        fontWeight: '500',
        color: 'white',
        textAlign: 'left',
        marginHorizontal: 16,
    },
    badgeWrapper: {
        position: 'absolute',
        top: -15,
        right: 5,
        padding: 5,
        paddingBottom: 3,
        paddingTop: 3,
        borderRadius: 20,
        backgroundColor: 'red',
        alignItems: 'center',
        justifyContent: 'center',
    },
    badgeText: {
        color: 'white',
        fontSize: 10,
    }
});

export default HeaderButtons;