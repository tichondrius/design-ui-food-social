import React, {Component} from 'react'
import {View, Text, ScrollView, Image, StyleSheet, TouchableNativeFeedback, ActivityIndicator, Linking, BackHandler} from 'react-native'
import { Entypo, FontAwesome, MaterialIcons } from '@expo/vector-icons';

import { Group, Item } from './group-item-menu'


export default class Drawler extends Component{
    constructor(props){
        super(props);
        
        this.state = {
            isOpenTypeStory: true,
            isCategoryGroupOpen: false,
            storyTypes: undefined
        };
        this._navigateTo = this._navigateTo.bind(this);
    }

    _navigateTo(pageName) {
        this.props.navigation.navigate(pageName)
    }
    render(){
        const  { navigate } = this.props.navigation;
        return (
            <View>
             <ScrollView>
                <View style={{width: '100%', height: 170, backgroundColor: 'skyblue'}}>
                    <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                        <Image
                        resizeMode='contain'
                        style={s.backgroundImage}
                        source={require('../images/drawer-navbar.jpeg')}
                        />
                    </View>
                </View>
                <View style={{marginTop: 10}}>
                    <Group 
                        canDropDown={false}
                        onTouch={() => {this._navigateTo('CreateFeedScreen')}} 
                        label="Thêm bài đăng" 
                        isOpen={this.state.isCategoryGroupOpen} 
                        icon={<Entypo name="new-message" size={25} color='#11c1f2'/>}>
                    </Group>
                    <Group 
                        canDropDown={false}
                        onTouch={() => {this._navigateTo('Search')}} 
                        label="Tìm kiếm" 
                        isOpen={this.state.isCategoryGroupOpen} 
                        icon={<FontAwesome name="search" size={25} color='#11c1f2'/>}>
                    </Group>
                    <Group 
                        canDropDown={false}
                        onTouch={() => {}} 
                        label="Cài đặt" 
                        isOpen={this.state.isCategoryGroupOpen} 
                        icon={<MaterialIcons name="settings" size={25} color='#11c1f2'/>}>
                    </Group>
                     <Group 
                        canDropDown={false}
                        onTouch={() => BackHandler.exitApp()} 
                        label="Thoát" 
                        isOpen={this.state.isCategoryGroupOpen} 
                        icon={<Entypo name="reply-all" size={25} color='#11c1f2'/>}>
                    </Group>
                    
                </View>
            </ScrollView>
            </View>
        );
       
    }
}

const s = StyleSheet.create({
  backgroundImage: {
      flex: 1,
      
  }
});