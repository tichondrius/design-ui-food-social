import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { StackNavigator, DrawerNavigator, createStackNavigator, TabNavigator, createDrawerNavigator, createBottomTabNavigator } from 'react-navigation';

import { Ionicons, MaterialIcons } from '@expo/vector-icons'
import { BaseButton } from 'react-native-gesture-handler';
import { NewFeedScreen, FriendListScreen,
  NearestScreen, CreateFeedScreen,
  SearchScreen, HotFeedScreen,
  FeedDetailScreen, UserInfoScreen,
  ViewImageScreen, } from './screens';
import Drawer from './shared/drawler';


console.disableYellowBox = true;


const TabTruyen = TabNavigator({
  NewFeedScreen: { screen: NewFeedScreen },
  HotFeedScreen: { screen: HotFeedScreen },
  FriendListScreen: { screen: FriendListScreen },
  NearestScreen: { screen: NearestScreen },
}, 
  {
 tabBarOptions: {
   showIcon: true,
   showLabel: true,
    tabStyle: {margin: 0, padding: 0, height: 50},
    style: {
    backgroundColor: '#32c5d2'
   },
 },
 tabBarPosition: 'bottom',

});



const StackTruyen = StackNavigator({
 Home: {screen: TabTruyen },
 Search: {screen: SearchScreen},
 CreateFeedScreen: { screen: CreateFeedScreen },
 FeedDetailScreen: { screen: FeedDetailScreen },
 UserInfoScreen: { screen: UserInfoScreen },
 ViewImageScreen: { screen: ViewImageScreen },
},{
  navigationOptions: {
    headerTitleStyle: { color: 'white' },
    headerStyle: {backgroundColor: '#11c1f2'},
    headerTintColor: 'white'
  }
});

const FoodSocialHome = DrawerNavigator({
 HomePage: {screen: StackTruyen},
}, {drawerWidth: 280, contentComponent: Drawer});

export default class App extends React.Component {
  render() {
    return (
      <FoodSocialHome />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
