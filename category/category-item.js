import FadeInView from '../shared/fade-in-view'
import React, { Component } from 'react';
import { TouchableOpacity, TouchableNativeFeedback, View, Image, Text } from 'react-native'
import { truncate } from '../libary'

import Toast, {DURATION} from 'react-native-easy-toast'

import { Entypo, FontAwesome, MaterialIcons, Ionicons } from '@expo/vector-icons';



const avatars = [
  require('../images/chibao.png'),
  require('../images/thanhlai.png'),
  require('../images/vanbui.png'),
  require('../images/baotrang.png'),
  require('../images/diemchau.png'),
];

const foodImages = [
  require('../images/1.png'),
  require('../images/2.png'),
  require('../images/3.png'),
  require('../images/4.png'),
  require('../images/5.png'),
];

export default class CategoryItem extends Component{
  constructor(props){
    super(props);
    this.state = {
      data: props.category,
    };
    
  }
  _onPressButton(){
    const {navigate} = this.props.navigation;
      navigate('FeedDetailScreen', {category: this.props.category});
  }

  toggleValue = (field) => {
    
    this.setState((prevState) => {
      const data = {
        ...prevState.data,
        // likeCount: field === ('like') ? (!prevState.data[field] ? prevState.data.likeCount + 1 : prevState.data.likeCount -1) : prevState.data.likeCount,
      };
      data[field] = !prevState.data[field];
      if (field === 'like') {
        if (!prevState.data.like) {
          data.likeCount = prevState.data.likeCount + 1;
        } else data.likeCount = prevState.data.likeCount -1;
      }
      this.refs.toast.show(`Bạn đã ${!prevState.data[field] ? '': 'bỏ'} ${field} bài viết`,DURATION.LENGTH_SHORT);
      return {
        data,
      };
    });
  }
  render(){
    const { data } = this.state;
    return (
      <FadeInView style={{height: 280, marginLeft: 3, marginTop: 3, marginRight: 3, borderRadius: 1, marginBottom: 3, elevation: 2, backgroundColor:'white'}}>
         <TouchableNativeFeedback 
            onPress={this._onPressButton.bind(this)}
          >
        <View  style = {{flex: 1, flexDirection: 'column', padding: 5}}>
        <Toast
            ref="toast"
            style={{backgroundColor:'black'}}
            position='top'
            positionValue={200}
            fadeInDuration={750}
            fadeOutDuration={1000}
            opacity={0.8}
            textStyle={{color:'white'}}
        />
          <View style={{height : 60, display: 'flex', flexDirection: 'row', alignItems: 'center'}}>
            <TouchableOpacity onPress={() => {this.props.navigation.navigate("UserInfoScreen", { NameUser: data.ownerName });}}>
              <Image source={avatars[data.avatar]} resizeMode='contain' style={{width: 50, height: 50, borderRadius: 50}} />
            </TouchableOpacity>
            <View style={{flex: 1, display: 'flex', marginLeft: 10, paddingTop: 7, paddingBottom: 7, flexDirection: 'column', justifyContent: 'space-evenly'}}>
              <View style={{flex: 1, display: 'flex', flexDirection: 'row', justifyContent: 'space-between'}}>
                <Text style={{ fontSize: 15, fontWeight: '600'}}>{data.ownerName}</Text>
                <Text>15 phút trước</Text>
              </View>
              <View>
                  <Text>{data.action} <Text style={{fontWeight: '600'}}>{data.foodName}</Text></Text>
              </View>
            </View>
          </View>
          
          <View style={{width: '100%', height: 170, backgroundColor: 'skyblue'}}>
            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
              <Image
                resizeMode='contain'
                style={{flex: 1}}
                source={foodImages[data.image]}
                />
            </View>
          </View>
          <View style={{flex: 1, display: 'flex', flexDirection: 'row'}}>
              <Ionicons.Button onPress={() => {this.toggleValue('like')}} size={25} backgroundColor="white" color={data.like ? 'red' : '#a6a6a6'} name="ios-heart"><Text>{data.likeCount}</Text></Ionicons.Button>
              <Entypo.Button onPress={() => {this.toggleValue('bookmark')}} size={25} backgroundColor="white" color={data.bookmark ? 'red': '#a6a6a6'} name="bookmark"/>  
              <MaterialIcons.Button onPress={() => {this.toggleValue('save')}} backgroundColor="white" size={25} color={data.save ? 'red': '#a6a6a6'} name="save"/>  
          </View>
          </View>
         </TouchableNativeFeedback>
      </FadeInView>
     
    );
  }  
}
